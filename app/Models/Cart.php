<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',

    ];

    public function products()
    {
        return $this->hasMany(CartProduct::class, 'cart_id');
    }

    public function getBy($userId) // lấy ra cart của 1 user_id
    {
        return Cart::whereUserId($userId)->first();
    }

    public function firtOrCreateBy($userId)
    {
        
        $cart = $this->getBy($userId);
        // dd($cart);
        if(!$cart)
        {
            $cart = $this->create(['user_id' => $userId]); // nếu chưa có cart thì tạo mới     
            
            // $cart = $this->cart->create(['user_id' => $userId]);
        }
        // dd($cart);
        return $cart; // nếu đã có cart thì trở về cart đó
    }

    public function getProductCountAttribute()
    {
        return auth()->check() ? $this->products->count() : 0;
    }

    public function getTotalPriceAttribute() // lấy tổng giá cả
    {
        return auth()->check() ? $this->products->reduce(function ($carry, $item) {
            $item->load('product');
            $price = $item->product_quantity * ($item->product->sale ? $item->product->sale_price : $item->product->price);
            return $carry + $price;
        }, 0) : 0;
    }


}
