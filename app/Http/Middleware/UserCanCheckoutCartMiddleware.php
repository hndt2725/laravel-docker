<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Cart;

class UserCanCheckoutCartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $cart = app(Cart::class)->firtOrCreateBy(auth()->user()->id);
        if($cart->products->count() > 0 )
        {
            return $next($request);
        } else {
            abort(404, 'No product in your cart');
        }
        
        
        
    }
}
