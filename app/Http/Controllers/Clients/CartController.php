<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\CreateOrderRequest;
use App\Http\Resources\Cart\CartResource;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;


class CartController extends Controller
{
    
    protected $cart;
    protected $product;
    protected $cartProduct;
    protected $coupon;
    protected $order;
    
    public function __construct(Product $product, Cart $cart, CartProduct $cartProduct, Coupon $coupon, Order $order )
    {
        $this->product = $product;
        $this->cart = $cart;
        $this->cartProduct = $cartProduct;
        $this->coupon = $coupon;
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $cart = $this->cart->firtOrCreateBy(auth()->user()->id)->load('products');

        return view('client.carts.index', compact('cart'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        {
            if($request->product_size) {
    
                $product = $this->product->findOrFail($request->product_id);
                $cart = $this->cart->firtOrCreateBy(auth()->user()->id);
                $cartProduct = $this->cartProduct->getBy($cart->id, $product->id, $request->product_size);
                if($cartProduct) {
                    $quantity = $cartProduct->product_quantity;
                    $cartProduct->update(['product_quantity' => ($quantity + $request->product_quantity)]);
                } else {
                    $dataCreate['cart_id'] = $cart->id;
                    $dataCreate['product_size'] = $request->product_size;
                    $dataCreate['product_quantity'] = $request->product_quantity ?? 1;
                    $dataCreate['product_price'] = $product->price;
                    $dataCreate['product_id'] = $request->product_id;
                    $this->cartProduct->create($dataCreate);
                }
                return back()->with(['message' => 'Added to cart success!']);
               } else {
                return back()->with(['message' => "You haven't chosen a size yet"]);
               }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    
    

    public function removeProductInCart($id)
    {
         $cartProduct =  $this->cartProduct->find($id);
         $cartProduct->delete();
         $cart =  $cartProduct->cart;
         return response()->json([
             'product_cart_id' => $id,
             'cart' => new CartResource($cart)
         ], Response::HTTP_OK);
    }



     /**
     * Update sản phẩm trong cart
     */
    public function updateQuantityProduct(Request $request, $id)
    {
        $cartProduct =  $this->cartProduct->find($id);
        $dataUpdate = $request->all();

        if($dataUpdate['product_quantity'] < 1 ) { // nếu số lượng <1 thì sẽ xóa nó đi
            $cartProduct->delete();
        } else {
            $cartProduct->update($dataUpdate); // còn nếu không thì update số lượng cho nó
        }

        $cart =  $cartProduct->cart; // update xong thì phải trả về cart

        return response()->json([
            'product_cart_id' => $id,
            'cart' => new CartResource($cart),
            'remove_product' => $dataUpdate['product_quantity'] < 1,
            'cart_product_price' => $cartProduct->total_price
        ], Response::HTTP_OK);
    }

    public function applyCoupon(Request $request)
    {
        $name = $request->input('coupon_code');

        $coupon =  $this->coupon->firstWithExperyDate($name, auth()->user()->id);

        if($coupon)
        {
            $message = 'Áp Mã giảm giá thành công !';
            Session::put('coupon_id', $coupon->id);
            Session::put('discount_amount_price', $coupon->value);
            Session::put('coupon_code' , $coupon->name);

        }else{

            Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);
            $message = 'Coupon code does not exist or has expired!';
        }

        return redirect()->route('client.carts.index')->with([
            'message' => $message,
        ]);
        
    }

    public function checkout()
    {
        $cart = $this->cart->firtOrCreateBy(auth()->user()->id)->load('products');

        return view('client.carts.checkout', compact('cart'));
    }

    public function processCheckout(CreateOrderRequest $request)
    {

        $dataCreate = $request->all();
        $dataCreate['user_id'] = auth()->user()->id;
        $dataCreate['status'] = 'pending';
        $this->order->create($dataCreate);

        $couponID = Session::get('coupon_id'); // xác định mã giảm giá user đã dùng
        if($couponID) 
        {
            $coupon =  $this->coupon->find(Session::get('coupon_id'));
            if($coupon)
            {
                $coupon->users()->attach(auth()->user()->id, ['value' => $coupon->value]); // thêm mới vào trong user
            }
        }
        $cart = $this->cart->firtOrCreateBy(auth()->user()->id);
        $cart->products()->delete(); // sau khi đã checkout thì xóa sản phẩm trong giỏ hàng
        Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']); // xóa các session đi

        // dd($dataCreate);
        // return redirect trang order
        return redirect()->route('client.orders.index')->with(['message' => 'Order success!']);
    }



}


