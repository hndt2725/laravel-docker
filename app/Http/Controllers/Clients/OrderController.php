<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        $orders = $this->order->getWithPaginateBy(auth()->user()->id); // lấy ra các order theo user_id
        return view('client.orders.index', compact('orders'));
    }

    public function cancelOrder($id)
    {
        $order = $this->order->findOrFail($id);
        $order->update(['status' => 'canceled']);
        return redirect()->route('client.orders.index')->with(['message' => 'Cancel success!']);

    }
}
