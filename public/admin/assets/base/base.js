function confirmDelete() {
    return new Promise((resolve, reject) => {
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.isConfirmed) {
                resolve(true);
            } else {
                reject(false);
            }
        });
    });
}
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});
$(() => {
    $(document).on("click", ".btn-delete", function (e) {
        e.preventDefault();
        let id = $(this).data("id");
        confirmDelete()
            .then(function () {

                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success') &&
                $(`#form-delete${id}`).submit();
            }).catch();
    });
});

// ///////////////////////

function confirmUpdate()
{
    return new Promise((resolve, reject) => {
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Save',
            denyButtonText: `Don't save`,
          }).then((result) => {
            if (result.isConfirmed) {
                resolve(true);
                Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
                reject(false);
                Swal.fire('Changes are not saved', '', 'info')
            }
        });
    });
}

$(() => {
    $(document).on("click", ".btn-update", function (e) {
        e.preventDefault();
        // let id = $(this).data("id");
        confirmUpdate()
            .then(function () {
                //  &&
                $('#confirmUpdate').submit()
            }).catch();
    });
});


$(() => {
    $(document).on("click", ".btn-create", function () {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                  })
                &&
                $('#createForm').submit()
            }).catch();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });