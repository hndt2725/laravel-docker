let sizes = [
    {
        id: Date.now(),
        size: "",
        quantity: 1,
    }
];
$(() => {
    console.log($('.btn-add-size'))
    loadFormSize()
    $('.btn-add-size').on('click', function () {
        console.log('Nhung đẹp gái')
        sizes.push({
            id: Date.now(),
            size: "",
            quantity: 1,
        })
        loadFormSize()
        // addSize();
        // appendSizesToForm();
    })
    changeInputSizes()
    reloadBtnRemove()
    // renderSizes(sizes);
    // appendSizesToForm();


    // $(Document).on("click", ".btn-remove-size", function () {
    //     let id = $(this).data("id");
    //     removeSize(sizes, id);
    //     appendSizesToForm();
    // });


    // $(Document).on("click", ".btn-add-size", function () {
    //     addSize();
    //     appendSizesToForm();
    // });


    // $(Document).on("keyup", ".input-size", function () {
    //     let id = $(this).data("id");
    //     let size = $(this).val();
    //     let index = getSizeIndex(sizes, id);
    //     console.log(1);
    //     if (index >= 0) {
    //         sizes[index].size = size;
    //     }
    //     appendSizesToForm();
    // });

    

    // $(Document).on("keyup", ".input-quantity", function () {
    //     let id = $(this).data("id")
    //     let quantity = $(this).val()
    //     let index = getSizeIndex(sizes, id)

    //     if (index >= 0) {
    //         sizes[index].quantity = quantity
    //     }
    // });

    

    // $("#image-input").change(function () {
    //     readURL(this);
    // });
    });

function changeInputSizes(){
    $('.input-size').each((index, item)=> {
        changePopupSize(item)
        changePopupSize($('.input-quantity')[index],'quantity')
    })
}
function changePopupSize(elm, checkType = 'size'){
    $(elm).on('keyup', function () {
        console.log(this)
        let listSize = sizes.map((value) => {
            if(value.id == $(this).attr('data-id')){
                if(checkType == 'size'){
                    value.size = $(this).val();
                }else{
                    value.quantity = Number($(this).val());
                }
            }
            return value;
        })
        sizes = listSize;
        appendSizesToForm()
    })
}
function reloadBtnRemove(){
 $('.btn-remove-size').each((index, item) => {
        $(item).on('click',() => {
            let idSize = $(item).attr('data-id');
            let listSizeNews = sizes.filter((value) => idSize != value.id)
            sizes = listSizeNews;
            loadFormSize()
            appendSizesToForm()
        })
    })
}
function loadFormSize(){
    let loadData = sizes.map((item, index) => {
        return renderSize(item);
    })
    $("#AddSizeModalBody").html(loadData);
    reloadBtnRemove()
    appendSizesToForm()
    changeInputSizes()
}
function renderSize(size) {
    return `<div class="product-item-size" id="product-size${size.id}">
                        <div class="row ">

                            <div class="input-group input-group-static col-5 w-40">
                                <label>Size</label>
                                <input value="${size.size}" type="text" id="input-size${size.id}" class="form-control input-size" data-id="${size.id}">
                            </div>

                            <div class="input-group input-group-static col-5 w-40">
                                <label>Quantity</label>
                                <input type="number" value="${size.quantity}" class="form-control input-quantity" data-id="${size.id}">
                            </div>

                            <div class="w-20">
                                <button type="button" class="btn btn-danger btn-remove-size" data-id="${size.id}">X</button>
                            </div>
                        </div>`;
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#show-image").attr("src", e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function renderSizes(sizes) {
    for (let size of sizes) {
        renderSize(size);
    }
}

function getSizeIndex(sizes, id) {
    let index = _.findIndex(sizes, function (o) {
        return o.id == id;
    });

    return index;
}

function removeSize(sizes, id) {
    let index = getSizeIndex(sizes, id);
    if (index >= 0) {
        $(`#product-size${sizes[index].id}`).remove();
        sizes.splice(index, 1);
    }
}

function addSize() {
    let size= {
        id: Date.now(),
        size: "30",
        quantity: 1,
    };
    sizes = [...sizes, size];
    renderSize(sizes);
}

function appendSizesToForm() {
    console.log(sizes)
    $("#inputSize").val(JSON.stringify(sizes));
}