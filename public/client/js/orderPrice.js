
getTotalOrderValue()

function getTotalOrderValue() {
    let total = $('.total-price').data('price');
    let couponPrice = $('.coupon-div')?.data('price') ?? 0;
    let shiping = $('.shipping').data('price');
    $('.total-order-price-all').text(`$${total + shiping - couponPrice}`);
    $('#total').val(total + shiping - couponPrice)
}

