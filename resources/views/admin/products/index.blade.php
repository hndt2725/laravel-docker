@extends('admin.layout.app')
@section('title', 'Products')
@section('content')
<div class="card">


    <h1>Product list</h1>

    @if (session('message'))
        <h5 class="text-primary">{{ session('message') }}</h5>
    @endif
    
    
    <div>
        <a href="{{ route('products.create') }}" class="btn btn-primary">Create</a>
    </div>
    <div >
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Image</th>
                <th>Name</th>
                <th>Price</th>
                <th>Sale</th>
                <th>Action</th>
            </tr>

            @foreach ($products as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><img src="{{ $item->images->count() > 0? asset('upload/' . $item->images->first()->url) :'upload/default.PNG' }}" width="100px" height="100px" alt=""></td>
                    
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->sale }}</td>
                    <td>
                        @can('update-product')
                            <a href="{{ route('products.edit', $item->id) }}" class="btn btn-outline-info" type="button">Edit</a>
                        @endcan
                        
                        @can('show-product')
                            <a href="{{ route('products.show', $item->id) }}" class="btn btn-outline-success" type="button">Show</a>
                        @endcan
                        
                        @can('delete-product')
                            <form action="{{ route('products.destroy', $item->id) }}" id="form-delete{{ $item->id }}" method="post">
                                    @csrf
                                    @method('delete')
                                    
                            </form>
                            <button class="btn btn-delete btn-outline-dark" type="submit" data-id={{ $item->id }}>Delete</button>
                        @endcan
                    </td>
                </tr>
                
            @endforeach

        </table>

        {{ $products->links() }}

    </div>

</div>

@endsection