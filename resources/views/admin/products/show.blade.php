@extends('admin.layout.app')
@section('title', 'Show Product')
@section('content')
    <div class="card">
        <h1>Show Product</h1>

        <div>

            <div class="row">
                <div class=" ">
                    <h6>Image</h6>
                    <div class="col-5">
                        <img style="width:200px; display:inline" src="{{ $product->images ? asset('upload/' . $product->images->first()->url) : 'upload/default.png' }}"
                            id="show-image" alt="">
                    </div>
                </div>

                <div class="">
                    <p><h6 style='display:inline'>Name:</h6> {{ $product->name }}</p>

                </div>

                <div class="">
                    <p><h6 style='display:inline'>Price:</h6> {{ $product->price }}</p>

                </div>

                <div class="">
                    <p><h6 style='display:inline'>Sale:</h6> {{ $product->sale }}</p>

                </div>

                <div class="form-group">
                    <h6>Description:</h6>
                    <p class="">
                        {!! $product->description !!}
                    </p>
                </div>

                <div>
                    <h6>Size:</h6>
                    @if ($product->details->count() > 0)
                        @foreach ($product->details as $detail)
                            <p>Size: {{ $detail->size }} - quantity: {{ $detail->quantity }}</p>
                        @endforeach
                    @else
                        <p> Sản phẩm này chưa nhập size</p>
                    @endif
                </div>

            </div>
            <div>
                <h6>Category:</h6>
                @foreach ($product->categories as $item)
                    <p>{{ $item->name }}</p>
                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection
