@extends('admin.layout.app')
@section('title', 'Edit coupon')
@section('content')
<div class="card">
     <h1>Edit Coupon</h1>

     <div>
        <form action="{{ route('coupons.update', $coupon->id) }}" id="confirmUpdate" method="post">
            @csrf
            @method('PUT')
            <div class="input-group input-group-static mb-4">
                <label>Name</label>
                <input type="text" value="{{ old('name') ?? $coupon->name }}" name="name" class="form-control text-uppercase">

                @error('name')
                    <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>

            <div class="input-group input-group-static mb-4">
                <label>Value</label>
                <input type="number" value="{{ old('value') ?? $coupon->value }}" name="value" class="form-control">

                @error('value')
                    <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>


            <div class="input-group input-group-static mb-4">
                <label name="group" class="ms-0">Type</label>
                <select name="type" class="form-control" >
                    
                    <option>-- Select Type --</option>
                    <option value="money" {{(old('type') ?? $coupon->type) == 'money' ? "selected" : ""}}  >Money</option>
                    <option value="freeship" {{(old('type') ?? $coupon->type) == 'freeship' ? "selected" : ""}}>Freeship</option>

                </select>

                @error('type')
                <span class="text-danger"> {{ $message }}</span>
            @enderror
            </div>

            <div class="input-group input-group-static mb-4">
                <label>Expery Date</label>
                <input type="date" value="{{ old('expery_date') ?? $coupon->expery_date }}" name="expery_date" class="form-control">

                @error('expery_date')
                    <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>



        </form>
        <button type="submit" class="btn btn-update btn-submit btn-primary">Update coupon</button>
    </div>
</div>
@endsection

