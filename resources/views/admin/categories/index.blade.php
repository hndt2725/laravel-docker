@extends('admin.layout.app')
@section('title', 'Categories')
@section('content')
<div class="card">

    @if (session('message'))
        <h5 class="text-primary">{{ session('message') }}</h5>
    @endif


    <h1>Category list</h1>
    <div>
        <a href="{{ route('categories.create') }}" class="btn btn-primary">Create</a>
    </div>
    <div >
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Parent Name</th>
                <th>Action</th>
            </tr>

            @foreach ($categories as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->parent_name }}</td>
                    <td>
                        @can('update-category')
                            <a href="{{ route('categories.edit', $item->id) }}" class="btn btn-outline-info" type="button">Edit</a>
                        @endcan

                        @can('delete-category')
                            <form action="{{ route('categories.destroy', $item->id) }}" id="form-delete{{ $item->id }}" 
                                method="post">
                                    @csrf
                                    @method('delete')
                            </form>
                            <button type = "submit" class="btn btn-delete btn-outline-dark" data-id={{ $item->id }}>Delete</button>
                        @endcan
                    </td>
                </tr>
                
            @endforeach

        </table>

        {{ $categories->links() }}

    </div>

</div>
@endsection

{{-- DELETE BUTTON------------------------------------------ --}}
