@extends('admin.layout.app')
@section('title', 'Roles')
@section('content')
<div class="card">
    
    <h1>Role list</h1>

    @if (session('message'))
        <h5 class="text-primary">{{ session('message') }}</h5>
    @endif


    <div>
        <a href="{{ route('roles.create') }}" class="btn btn-primary">Create</a>
    </div>
    <div >
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Display Name</th>
                <th>Action</th>
            </tr>

            @foreach ($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->display_name }}</td>
                    <td>
                        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-outline-info" type="button">Edit</a>
                        
                        <form action="{{ route('roles.destroy', $role->id) }}" id="form-delete{{ $role->id }}" method="post">
                                @csrf
                                @method('delete')
                                
                        </form>
                        <button class="btn btn-delete btn-outline-dark" data-id={{ $role->id }}>Delete</button>

                    </td>
                </tr>
                
            @endforeach

        </table>

        {{ $roles->links() }}

    </div>

</div>

@endsection