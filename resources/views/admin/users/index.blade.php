@extends('admin.layout.app')
@section('title', 'Users')
@section('content')
<div class="card">


    <h1>User list</h1>

    @if (session('message'))
        <h5 class="text-primary">{{ session('message') }}</h5>
    @endif
    
    
    <div>
        <a href="{{ route('users.create') }}" class="btn btn-primary">Create</a>
    </div>
    <div >
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Image</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>

            @foreach ($users as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><img src="{{ $item->images->count() > 0? asset('upload/'.$item->images->first()->url) :'upload/default.PNG' }}" width="100px" height="100px" alt=""></td>
                    {{-- ->first() --}}
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>
                        @can('update-user')
                        <a href="{{ route('users.edit', $item->id) }}" class="btn btn-outline-info" type="button">Edit</a>
                        @endcan

                        @can('delete-user')
                            <form action="{{ route('users.destroy', $item->id) }}" id="form-delete{{ $item->id }}" method="post">
                                    @csrf
                                    @method('delete')
                                    
                            </form>
                            <button class="btn btn-delete btn-outline-dark" type="submit" data-id={{ $item->id }}>Delete</button>
                        @endcan
                    </td>
                </tr>
                
            @endforeach

        </table>

        {{ $users->links() }}

    </div>

</div>

@endsection