@extends('admin.layout.app')
@section('title', 'Orders')
    
@section('content')

<div class="card">
    <h1>Orders</h1>
    <div class="container-fluid pt-5">      
        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif

        <div class="col">
            <div>
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th>Ship</th>
                        <th>Customer name</th>
                        <th>Customer email</th>
                        <th>Customer address</th>
                        <th>Note</th>
                        <th>Payment</th>


                    </tr>

                    @foreach ($orders as $item)
                        <tr>
                            <td>{{ $item->id }}</td>

                            <td>
                                {{ $item->status }}
                                 {{-- <div class="input-group input-group-static mb-4">
                                    <select name="status" class="form-control select-status"
                                        data-action="{{ route('admin.orders.update_status', $item->id) }}">
                                        @foreach (config('order.status') as $status)
                                        
                                            <option value="{{ $status }}"
                                           
                                                {{ $status == $item->status ? "selected" : "" }}>{{ $status }}
                                            </option>
                                            @php
                                                dd($item->status);
                                            @endphp
                                        @endforeach
                                    </select>  --}}
                            </td>

                            <td>${{ $item->total }}</td>

                            <td>${{ $item->ship }}</td>
                            <td>{{ $item->customer_name }}</td>
                            <td>{{ $item->customer_email }}</td>

                            <td>{{ $item->customer_address }}</td>
                            <td>{{ $item->note }}</td>
                            <td>{{ $item->payment }}</td>
                        </tr>
                    @endforeach
                </table>
                {{ $orders->links() }}
            </div>
        </div>

    </div>
</div>
{{-- <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{ asset('admin/assets/order/order.js') }}"></script> --}}
@endsection