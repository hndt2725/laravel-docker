<div class="container-fluid bg-secondary text-dark mt-5 pt-5">
    <div class="row px-xl-5 pt-5">
        <div class="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5">
            <a href="" class="text-decoration-none">
                <h1 class="mb-4 display-5 font-weight-semi-bold"><span
                        class="text-primary font-weight-bold border border-white px-3 mr-1">E</span>Shop</h1>
            </a>
            <p>Trang web này được edit bởi Hồng Nhung.</p>
            <p class="mb-2"><i class="fa fa-map-marker-alt text-primary mr-3"></i>Minh Đức, Thủy Nguyên, Hải Phòng
                USA</p>
            <p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>hndt2725@gmail.com</p>
            <p class="mb-0"><i class="fa fa-phone-alt text-primary mr-3"></i>+012 345 67890</p>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="row">
                <div class="col-md-4 mb-5">
                    <h5 class="font-weight-bold text-dark mb-4">Về Eshop</h5>
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-dark mb-2" href="index.html"><i class="fa fa-angle-right mr-2"></i>
                            Giới Thiệu Về EShop</a>
                        <a class="text-dark mb-2" href="shop.html"><i class="fa fa-angle-right mr-2"></i>
                            Tuyển Dụng</a>
                        <a class="text-dark mb-2" href="detail.html"><i class="fa fa-angle-right mr-2"></i>
                            Điều Khoản Shopee</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Chính Sách Bảo Mật</a>
                        <a class="text-dark mb-2" href="checkout.html"><i class="fa fa-angle-right mr-2"></i>
                            Chính Hãng</a>
                        <a class="text-dark mb-2" href="checkout.html"><i class="fa fa-angle-right mr-2"></i>
                            Kênh Người Bán</a>
                        <a class="text-dark mb-2" href="contact.html"><i class="fa fa-angle-right mr-2"></i>Contact
                            Chương Trình Tiếp Thị Liên Kết EShop</a>
                        <a class="text-dark mb-2" href="contact.html"><i class="fa fa-angle-right mr-2"></i>Contact
                            Liên Hệ Với Truyền Thông</a>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <h5 class="font-weight-bold text-dark mb-4">CHĂM SÓC KHÁCH HÀNG</h5>
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-dark mb-2" href="index.html"><i class="fa fa-angle-right mr-2"></i>Trung Tâm Trợ Giúp</a>
                        <a class="text-dark mb-2" href="shop.html"><i class="fa fa-angle-right mr-2"></i>
                            EShop Blog</a>
                        <a class="text-dark mb-2" href="detail.html"><i class="fa fa-angle-right mr-2"></i>
                            EShop Mall</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Hướng Dẫn Mua Hàng</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Hướng Dẫn Bán Hàng</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Thanh Toán</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Vận Chuyển</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Trả Hàng & Hoàn Tiền</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Chăm Sóc Khách Hàng</a>
                        <a class="text-dark mb-2" href="cart.html"><i class="fa fa-angle-right mr-2"></i>
                            Chính Sách Bảo Hành</a>

                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <h5 class="font-weight-bold text-dark mb-4">Newsletter</h5>
                    <form action="">
                        <div class="form-group">
                            <input type="text" class="form-control border-0 py-4" placeholder="Your Name"
                                required="required" />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control border-0 py-4" placeholder="Your Email"
                                required="required" />
                        </div>
                        <div>
                            <button class="btn btn-primary btn-block border-0 py-3" type="submit">Subscribe
                                Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row border-top border-light mx-xl-5 py-4">
        <div class="col-md-6 px-xl-0">
            <p class="mb-md-0 text-center text-md-left text-dark">
                &copy; <a class="text-dark font-weight-semi-bold" href="https://www.facebook.com/hndt2725/">HNDT2725</a>
                <br>
                Distributed By <a href="https://www.facebook.com/hndt2725/" target="_blank">Hồng Nhung</a>
            </p>
        </div>
        <div class="col-md-6 px-xl-0 text-center text-md-right">
            <h6>Thanh toán</h6>
            <img class="img-fluid" src="{{ asset('client/img/payments.png') }}" alt="">
        </div>
    </div>
</div>

