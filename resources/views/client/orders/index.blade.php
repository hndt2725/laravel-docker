<!-- Featured Start -->
@extends('client.layouts.app')
@section('title', 'Home')
@section('content')
    <div class="container-fluid pt-5">
        
        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif


        <div class="col">
            <div>
                <table class="table table-hover">
                    <tr>
                        <th>#</th>

                        <th>status</th>
                        <th>total</th>
                        <th>ship</th>
                        <th>customer_name</th>
                        <th>customer_email</th>
                        <th>customer_address</th>
                        <th>note</th>
                        <th>payment</th>


                    </tr>

                    @foreach ($orders as $item)
                        <tr>
                            <td>{{ $item->id }}</td>

                            <td>{{ $item->status }}</td>
                            <td>${{ $item->total }}</td>

                            <td>${{ $item->ship }}</td>
                            <td>{{ $item->customer_name }}</td>
                            <td>{{ $item->customer_email }}</td>

                            <td>{{ $item->customer_address }}</td>
                            <td>{{ $item->note }}</td>
                            <td>{{ $item->payment }}</td>
                            <td>
                                @if ($item->status == 'pending')
                                    <form action="{{ route('client.orders.cancel', $item->id )}}"
                                        id="form-cancel{{ $item->id }}" method="get">
                                        {{-- method = get --}}
                                        @csrf
                                        {{-- @method('GET') --}}
                                        <button class="btn btn-cancel btn-danger" data-id={{ $item->id }}>Cancle
                                            Order</button>
                                    </form>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </table>
                {{ $orders->links() }}
            </div>
        </div>

    </div>

    <script
src="https://code.jquery.com/jquery-3.7.0.js"
integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
crossorigin="anonymous"></script>
<script src="/client/js/cancelOrder.js"></script>
@endsection
{{-- @section('script')
    <script>

    </script>

@endsection --}}
