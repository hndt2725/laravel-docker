<?php 

return [ 
    'status' => [
        'Accept' => 'Accept',
        'Pending' => 'Pending',
        'Delivery' => 'Delivery',
        'Success' => 'Success',
        'Canceled' => 'Canceled',
    ]
];